<?php

class WGPAdmin
{

    public function __construct()
    {
        $this->addActions();
    }

    /**
     * @return void
     */
    public function addActions(){
        add_action('admin_menu', [$this,'adminMenu']);
        add_action( 'admin_enqueue_scripts', [ $this, 'addScripts' ] );
    }

    /**
     * @return void
     */
    public function adminMenu(){
        //create menu
        add_menu_page(
            __('Woocommerce PayUnit Gateway Transactions','woo-gateway-payunit'),
            __('WGP Options','woo-gateway-payunit'),
            'manage_options',
            WOO_PAYUNIT_MENU_SLUG,
            '',
            'dashicons-editor-contract',
            60
        );
        require_once  WOO_PAYUNIT_DIR.'inc/admin/submenu.php';
        $sub_menus = wg_payunit_submenu_options();
        foreach ($sub_menus as $sub_menu){
            add_submenu_page(
                $sub_menu['parent_slug'],
                $sub_menu['page_title'],
                $sub_menu['menu_title'],
                $sub_menu['capability'],
                $sub_menu['menu_slug'],
                $sub_menu['function'],
                $sub_menu['position']
            );
        }

    }

    /**
     * @return void
     */
    public function addScripts(){

        wp_enqueue_style('wg-payunit-style', WOO_PAYUNIT_URL . '/assets/css/style.css', [], '1.0');
    }
}