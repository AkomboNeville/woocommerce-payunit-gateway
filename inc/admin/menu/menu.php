<?php

function wg_payunit_options(){
    return [
        'page_title'=>'Woocommerce Gateway PayUnit Transactions',
        'menu_title'=>'WGP Options',
        'capability'=>'manage_options',
        'menu_slug'=> WOO_PAYUNIT_MENU_SLUG,
        'callback' => '',
        'icon_url' => 'dashicons-editor-contract',
        'position' => 60
    ];
}
