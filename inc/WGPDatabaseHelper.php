<?php


class WGPDatabaseHelper
{

    private string $table_name;
    private string $table = 'wgp_transactions';
    private string $charset_collate;
    private string $where;
    private int $per_page;
    private int $current_page;

    public function __construct()
    {
        global $wpdb;
        $this->table_name = $wpdb->prefix . $this->table;
        $this->charset_collate = $wpdb->get_charset_collate();
        $this->where = '';
    }


    public function createTable()
    {

        $sql = "CREATE TABLE $this->table_name (
          id bigint(20) NOT NULL AUTO_INCREMENT,
          user_id bigint(20) NOT NULL,
          order_id bigint(20) NOT NULL,
          transaction_id varchar(100) NOT NULL,
          currency varchar(25) NOT NULL, 
          total_amount text NOT NULL,
          return_url varchar(100) DEFAULT '' NOT NULL,
          notify_url varchar(100),
          payment_method varchar(100) DEFAULT 'payunit',
          status ENUM('failed','completed') DEFAULT 'failed',
          created_at datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
          updated_at datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,          
          PRIMARY KEY  (id)
        ) $this->charset_collate;";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);
    }

    /**
     * @param array $fields value pair
     * @return bool|int|mysqli_result|resource|null
     */
    public function save($fields)
    {
        global $wpdb;
        $fields['created_at'] = date("Y-m-d H:i");
        $fields['updated_at'] = date("Y-m-d H:i");
        $wpdb->insert($this->table_name, $fields);
        return $wpdb->insert_id;
    }

    public function find($id)
    {
        return $this->where("id", "=", $id)->first();
    }

    public function where( $field, $comparison, $value)
    {
        $this->where = " WHERE " . $this->table_name . '.' . $field . " " . $comparison . " " . $value;
        return $this;
    }

    public function first()
    {
        $data = $this->get();
        if (sizeof($data)) {
            return $data[0];
        }
        return null;
    }


    /**
     * @param array $fields
     * @param bool $distinct
     * @return array|object|stdClass[]
     */
    public function get($fields = [],$distinct = false)
    {

        global $wpdb;
        $select = $distinct ? "SELECT DISTINCT * " : "SELECT * "; // set select all as the default
        if (sizeof($fields) > 0) { // we want to get specific fields, not everything, eg only id, name
            $select = $distinct ? 'SELECT DISTINCT ' : 'SELECT ';
            foreach ($fields as $field) {
                $select .= $field . ', ';
            }
            $select = rtrim($select, ', '); // removes the last comma (,) from the select statement
        }
        $query = $select . " FROM " . $this->table_name;
        $additions = $this->where;
        $additions .= " ORDER BY " . $this->table_name . '.' . 'id' . " " . 'desc';
        if ( $this->per_page > 0 ) { // check if the query requires pagination
            $total_query = 'SELECT COUNT(*) as total FROM ' . $this->table_name . $additions;
            if ($distinct) {
                $total_query = ' SELECT COUNT(DISTINCT ' . $this->table_name . '.' . 'id' . ') as total FROM ' . $this->table_name . $additions;
            }
            $total = intval($wpdb->get_var($total_query));
            $query .= $additions;
            $offset         = ( $this->current_page * $this->per_page ) - $this->per_page;
            $query          .= " LIMIT " . $offset . ' , ' . $this->per_page;


            $data           = $this->getResults( $query );
            $total_pages    = $total ? ceil( $total / $this->per_page) : 0;
            $total_pages    = $total_pages > round( $total_pages ) ? round( $total_pages ) + 1 : round( $total_pages );
            $data           = [
                'data'       => $data,
                'page'       => $this->current_page,
                'total_pages' => $total_pages,
                'perPage'    => $this->per_page,
                'total_items' => $total
            ];
        }else{
            $query .= $additions;
            $data = $this->getResults($query);
        }

        $wpdb->flush();
        $this->where = '';
        return $data;
    }
    public function paginate( $per_page = 0,$current_page = 1 ) {
        $this->per_page     = $per_page;
        $this->current_page = $current_page;
        return $this;
    }

    /**
     * @param $query
     * @return array|object|stdClass[]
     */
    private function getResults($query)
    {
        global $wpdb;
        $results = $wpdb->get_results($query);
        $data = [];
        if ($results) {
            $data = $results;
        }
        return $data;
    }
    public function update($id,$fields){
        global $wpdb;
        return $wpdb->update($this->table_name,$fields,['id'=>$id]);
    }

}